#include <cstdio>
#include <stack>

int main(){
    int n;
    scanf("%d", &n);

    int tmpCur = 0;
    bool cheater = false;
    std::stack<int> storage;

    for(int i = 0; i < n; i++){
        int cur;
        scanf("%d", &cur);

        if(tmpCur < cur){
            for(int j = tmpCur+1; j < cur; j++){
                storage.push(j);
            }
            tmpCur = cur;
        }else{
            if(cur == storage.top()){
                storage.pop();
            }else{
                cheater = true;
            }
        }
    }

    if(cheater){
        printf("Cheater\n");
    }else{
        printf("Not a proof\n");
    }

    return 0;
}
