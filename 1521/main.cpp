#include <cstdio>
#include <algorithm>

int* tree;

void build(int i, int treeLeft, int treeRight){
    if(treeLeft == treeRight){
        tree[i] = 1;
    }else{
        int middle = (treeLeft + treeRight) >> 1;
        int i2 = i << 1;
        build(i2, treeLeft, middle);
        build(i2+1, middle+1, treeRight);
        tree[i] = tree[i2] + tree[i2+1];
    }
}

void update(int i, int treeLeft, int treeRight, int pos) {
    if (treeLeft == treeRight){
        tree[i] = 0;
    }else {
        int middle = (treeLeft + treeRight) >> 1;
        int i2 = i << 1;
        if (pos <= middle){
            update(i2, treeLeft, middle, pos);
        }else {
            update(i2+1, middle+1, treeRight, pos);
        }
        tree[i] = tree[i2] + tree[i2+1];
    }
}

int sum (int i, int treeLeft, int treeRight, int left, int right) {
    if (left > right) {
        return 0;
    }
    if (left == treeLeft && right == treeRight){
        return tree[i];
    }
    int middle = (treeLeft + treeRight) >> 1;
    int i2 = i << 1;
    return sum (i2, treeLeft, middle, left, std::min(right, middle)) + 
            sum (i2+1, middle+1, treeRight, std::max(left, middle+1), right);
}

int find_kth(int i, int treeLeft, int treeRight, int k) {
    if (treeLeft == treeRight)
        return treeLeft;
    int middle = (treeLeft + treeRight) >> 1;
    int i2 = i << 1;
    if (tree[i2] >= k)
        return find_kth(i2, treeLeft, middle, k);
    else
        return find_kth(i2 + 1, middle + 1, treeRight, k - tree[i2]);
}

int main(){
    int n, k;
    scanf("%d %d", &n, &k);
    
    tree = new int[4*n];
    
    build(1, 0, n-1);
    
    int prev = 0;
    for(int i = 0; i < n; i++){
        int sumAll = sum(1, 0, n-1, 0, n-1);
        int sumRight = sum(1, 0, n-1, prev, n-1);
        int sumLeft = sum(1, 0, n-1, 0, prev-1);

        if(sumRight >= k){
            prev = sumLeft + k;
        }else{
            prev = k - sumRight;
            if(prev >= sumAll){
                int tmp = prev % sumAll;
                prev = tmp == 0 ? sumAll : tmp;
            }
        }

        int pos = find_kth(1, 0, n-1, prev);
        printf("%d ", pos+1);
        update(1, 0, n-1, pos);
        prev = pos;
    }
    return 0;
}
