#include <string>
#include <iostream>
#include <map>

struct Folder {
    Folder() {
        this->name = "";
        this->child = {};
    }
    Folder(std::string name){
        this->name = name;
        this->child = {};
    }

    std::string name;
    std::map<std::string, Folder*> child;
};


void print_folders(const Folder * root, int level){
    for(int i = 0; i < level-1; i++){
        printf(" ");
    }

    if(!root->name.empty()){
        std::cout << root->name << std::endl;
    }

    level++;
    for(auto &child : root->child){
        print_folders(child.second, level);
    }

}

int main(){
    int n;
    std::cin >> n;

    Folder* root = new Folder();

    for(int i = 0; i < n; i++){
        std::string path, dir;
        std::cin >> path;

        Folder* cur = root;

        for(size_t j = 0; j <= path.size(); j++){
            if(path[j] == '\\' || path[j] == '\0'){
                auto dir_tree = cur->child.find(dir);

                if(dir_tree == cur->child.end()){
                    Folder *f = new Folder(dir);
                    cur->child[dir] = f;
                    cur = cur->child.find(dir)->second;
                }else{
                    cur = dir_tree->second;
                }
                dir = "";
            }else{
                dir += path[j];
            }
        }
    }

    print_folders(root, 0);

    return 0;
}
