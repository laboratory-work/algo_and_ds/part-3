#include <cstdio>
#include <string>
#include <map>
#include <set>
#include <functional>
#include <iostream>
#include <bits/stdc++.h>

#define ll long long
using namespace std;

struct City
{
    string name;
    ll money;
    int days;
};

struct Person
{
    ll money;
    City* loc;
};

City city[60000];
Person person[10000];

map<string, Person*> persons;
map<string, City*> cities;

set<pair<ll, City*>, greater<>> score;

int main(){
    ios_base::sync_with_stdio(false);

    int n;
    scanf("%d", &n);
    
    int cnt = 0;
    for (int i = 0; i < n; i++) {
        string name_person;
        string name_city;
        ll money;
        cin >> name_person >> name_city >> money;

        if (!cities[name_city]) {
            city[cnt].name = name_city;
            city[cnt].money = money;
            cities[name_city] = &city[cnt];
            cnt++;
        } else {
            cities[name_city]->money += money;
        }

        person[i].money = money;
        person[i].loc = cities[name_city];
        persons[name_person] = &person[i];
    }

    for (auto &item : cities) {
        score.insert({item.second->money, item.second});
    }

    int m, k, today = 0;
    cin >> m >> k;

    for (int i = 0; i < k; i++) {
        int day;
        string name_person;
        string name_city;
        cin >> day >> name_person >> name_city;

        int count = day - today;
        today = day;

        auto it2 = score.begin();
        auto it = it2++;

        if (it2 == score.end() || it->first > it2->first) {
            it->second->days += count;
        }
        
        City *to_city = cities[name_city];
        Person *who = persons[name_person];

        if (to_city == nullptr) {
            city[cnt].name = name_city;
            cities[name_city] = &city[cnt];
            cnt++;

            to_city = cities[name_city];
        }
        
        score.erase({who->loc->money, who->loc});
        score.erase({to_city->money, to_city});
        who->loc->money -= who->money;
        score.insert({who->loc->money, who->loc});
        who->loc = to_city;
        to_city->money += who->money;
        score.insert({to_city->money, to_city});
    }

    int count = m - today;
    auto it2 = score.begin();
    auto it = it2++;
    if (it2 == score.end() || it->first > it2->first) {
        it->second->days += count;
    }

    for (auto &item : cities) {
        if (item.second->days > 0) {
            cout << item.first << " " << item.second->days << endl;
        }
    }

    return 0;
}
