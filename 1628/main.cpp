#include <cstdio>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct Point {
    int xi;
    int yi;
};

bool comp1(Point a, Point b) {
    if (a.xi != b.xi) {
        return a.xi < b.xi;
    } else {
        return a.yi < b.yi;
    }
}

bool comp2(Point a, Point b) {
    if (a.yi != b.yi) {
        return a.yi < b.yi;
    } else {
        return a.xi < b.xi;
    }
}

int main(){
    
    int m, n, k;
    int res = 0;

    scanf("%d %d %d", &m, &n, &k);

    vector<Point> point;
    vector<Point> single_point;

    for (int i = 0; i < k; i++) {
        int xi, yi;
        scanf("%d %d", &xi, &yi);
        xi--; yi--;
        point.push_back({xi, yi});
    }

    for (int i = 0; i < m; i++) {
        point.push_back({i, n});
        point.push_back({i, -1});
    }

    for (int i = 0; i < n; i++) {
        point.push_back({m, i});
        point.push_back({-1, i});
    }

    sort(point.begin(), point.end(), comp1);

    for (int i = 0; i < point.size() - 1; i++) {
        if (point[i].xi == point[i + 1].xi) {
            if (point[i + 1].yi - point[i].yi - 1 > 1) {
                res++;
            } else if (point[i + 1].yi - point[i].yi - 1 == 1) {
                single_point.push_back({point[i].xi, point[i].yi + 1});
            }
        }
    }

    sort(point.begin(), point.end(), comp2);

    for (int i = 0; i < point.size() - 1; i++) {
        if (point[i].yi == point[i + 1].yi) {
            if (point[i + 1].xi - point[i].xi - 1 > 1) {
                res++;
            } else if (point[i + 1].xi - point[i].xi - 1 == 1) {
                single_point.push_back({point[i].xi + 1, point[i].yi});
            }
        }
    }

    sort(single_point.begin(), single_point.end(), comp1);

    if (single_point.size() > 1) {
        for (int i = 0; i < single_point.size() - 1; i++) {
            if (single_point[i].xi == single_point[i + 1].xi && single_point[i].yi == single_point[i + 1].yi) {
                res++;
                i++;
            }
        }
    }

    printf("%d", res);

    return 0;
}
